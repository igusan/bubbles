#Манифест

**Здесь будет репозиторий проекта доступо-центричной операционной системы.**
**просто поток неформатированной мысли, будет переписано:**

Пузыри - это мобильная операционная система, главная концепция которой - это полная изоляция приложений в пузырях c централизованным и понятным контролем их доступа. Современные мобильные ОС, делают настройки прав приложений сложными и нелокальными, потакая недобросовестным разработчикам ПО в сборе всех возможных данных. Даже продвинутый пользователь не в состоянии уследить за всеми настройками устанавливаемых приложений. Пузыри идет от обратного.

Основная идея в том, что новое приложение при установкеи оказывается в пузыре, не видит хранилище данных, не имеет доступа в интернет. При установке пользователь может по быстрому назначить приложению одну из групп прав, или вручную настроить все доступы. Это требование истекает из того, что обычно пользователь сначала устанавливает приложение и только потом отключает ему интернет, обновления, доступы, к этому времени недобросоветная прога могла уже отчитаться на сервер, рассказав все, что знает и запросив программу действий.


*Курсив*

---

## Необходимо строго контролировать права программы по следующим пунктам (и не только)

- Способность программы изменять себя, включая настройки.
- Доступ к диску на чтение и запись
- доступ в интернет
- доступ к информации об окружающей среде:
	- Другие программы
	- версия и настройки ОС
	- возможность отобразить вэб-контент или вызвать отображение вэб-контента в браузере
	- сенсоры устройства: гироскоп и акселерометр, позиционирование, камера, микрофон...
	- доступ в сеть
Все настройки должны быть снабжены комментариями, ясно излогающими сущность вопроса, не общими формулировками

Сама система, в свою очередь, должена себя вести не менее безолаберно. Обновления должны быть по умолчанию выключены, а так-же все возможные отчеты об ошибках. Отправка должна происходить только с осознанного согласия пользователей.

---
1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).